﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using empresas_dotNET.Model;

namespace empresas_dotNET.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilmesController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public FilmesController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: api/Filmes/Diretor
        [HttpGet("diretor/{diretor}")]
        public async Task<ActionResult<IEnumerable<Filme>>> GetFilmeDiretor(string diretor)
        {

            return await _context.Filmes
                                        .Include(f => f.FilmeAtor)
                                        .ThenInclude(f => f.Ator)
                                        .Where(f => f.Diretor == diretor)
                                        .ToListAsync();
        }

        // GET: api/Filmes/Nome
        [HttpGet("nome/{nome}")]
        public async Task<ActionResult<IEnumerable<Filme>>> GetFilmeNome(string nome)
        {

            return await _context.Filmes
                                        .Include(f => f.FilmeAtor)
                                        .ThenInclude(f => f.Ator)
                                        .Where(f => f.Nome == nome)
                                        .ToListAsync();
        }

        // GET: api/Filmes/GeneroAtor
        [HttpGet("generoAtor/{genero}/{ator?}")]
        public async Task<ActionResult<IEnumerable<Filme>>> GetFilmeGeneroAtor(string genero, string ator)
        {
            if (ator == null)
            {
                return await _context.Filmes
                                            .Include(f => f.FilmeAtor)
                                            .ThenInclude(f => f.Ator)
                                            .Where(f => f.Genero == genero)
                                            .ToListAsync();
            }
            else
            {
                return await _context.Filmes
                                            .Include(f => f.FilmeAtor)
                                            .ThenInclude(f => f.Ator)
                                            .Where(f => (f.Genero == genero && f.FilmeAtor.First().Ator.Nome == ator))
                                            .ToListAsync();
            }

        }

        // GET: api/Filmes/Ator
        [HttpGet("ator/{ator}")]
        public async Task<ActionResult<IEnumerable<Filme>>> GetFilmeAtor(string ator)
        {

                return await _context.Filmes
                                            .Include(f => f.FilmeAtor)
                                            .ThenInclude(f => f.Ator)
                                            .Where(f => f.FilmeAtor.First().Ator.Nome == ator)
                                            .ToListAsync();

        }



        // GET: api/Filmes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Filme>> GetFilme(Guid id)
        {
            var filme = await _context.Filmes.FindAsync(id);

            if (filme == null)
            {
                return NotFound();
            }

            return filme;
        }

        // PUT: api/Filmes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFilme(Guid id, Filme filme)
        {
            if (id != filme.Id)
            {
                return BadRequest();
            }

            _context.Entry(filme).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FilmeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Filmes
        [HttpPost]
        public async Task<ActionResult<Filme>> PostFilme(Filme filme)
        {
            //if (!usuario.Adm)
            //    throw new Exception("O Usuário deve ser um administrador para cadastrar um filme");
            _context.Filmes.Add(filme);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFilme", new { id = filme.Id }, filme);
 
        }

        // DELETE: api/Filmes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Filme>> DeleteFilme(Guid id)
        {
            var filme = await _context.Filmes.FindAsync(id);
            if (filme == null)
            {
                return NotFound();
            }

            _context.Filmes.Remove(filme);
            await _context.SaveChangesAsync();

            return filme;
        }

        private bool FilmeExists(Guid id)
        {
            return _context.Filmes.Any(e => e.Id == id);
        }
    }
}
