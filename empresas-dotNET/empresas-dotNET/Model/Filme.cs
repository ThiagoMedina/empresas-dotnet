﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.Model
{
    public class Filme
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Campo 'Nome' é obrigatorio.")]
        [StringLength(100, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Campo 'Diretor' é obrigatorio.")]
        [StringLength(100, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Diretor { get; set; }

        [Required(ErrorMessage = "Campo 'Gênero' é obrigatorio.")]
        [StringLength(100, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Genero { get; set; }
        public IEnumerable<Voto> Voto { get; set; }

        public IEnumerable<FilmeAtor> FilmeAtor { get; set; }

    }
}
