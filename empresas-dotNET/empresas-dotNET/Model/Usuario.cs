﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.Model
{
    public class Usuario
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Campo 'Nome' é obrigatorio.")]
        [StringLength(100, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Campo 'Email' é obrigatorio.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail invalido.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Campo 'Password' é obrigatorio.")]
        [DataType(DataType.Password)]
        [StringLength(10, ErrorMessage = "A senha deve ter entre 6 e 10 caracteres", MinimumLength = 6)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Campo 'ConfirmPassword' é obrigatorio.")]
        [DataType(DataType.Password)]  
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "A senha e a senha de confirmação não correspondem.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Campo 'Adm' é obrigatorio.")]
        public bool Adm { get; set; }

        public IEnumerable<Voto> Voto { get; set; }

    }
}
