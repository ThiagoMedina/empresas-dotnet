﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.Model
{
    public class Ator
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Campo 'Nome' é obrigatorio.")]
        [StringLength(100, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Campo 'Genero' é obrigatorio.")]
        public string GeneroPrincipal { get; set; }

        [Required(ErrorMessage = "Campo 'Idade' é obrigatorio.")]
        public int Idade { get; set; }

        [Required(ErrorMessage = "Campo 'Biografia' é obrigatorio.")]
        public string Biografia { get; set; }

        public IEnumerable<FilmeAtor> FilmeAtor { get; set; }
    }
}
