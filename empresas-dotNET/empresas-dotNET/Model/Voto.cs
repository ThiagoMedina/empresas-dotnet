﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.Model
{
    public class Voto
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Campo 'Nome' obrigatorio.")]
        public virtual Filme Filme { get; set; }

        [Required(ErrorMessage = "Campo 'Usuario' obrigatorio.")]
        public virtual Usuario Usuario { get; set; }

        [Required(ErrorMessage = "Campo 'Nota' obrigatorio.")]
        [Range(0, 4, ErrorMessage = "A avaliação deve ser de 0 a 4.")]
        public int Nota { get; set; }
    }
}
