﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotNET.Model
{
    public class FilmeAtor
    {
        [Key]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Campo 'Filme' obrigatorio.")]
        public Filme Filme { get; set; }
        [Required(ErrorMessage = "Campo 'Ator' obrigatorio.")]
        public Ator Ator { get; set; }
    }
}
